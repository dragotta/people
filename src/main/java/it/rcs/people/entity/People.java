package it.rcs.people.entity;

import java.util.List;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import it.rcs.people.model.dto.Address;

@Document
public class People {

	@Id
	private String peopleId;
	private String name;
	private String surname;
	private String profession;
	private List<Address> address;
	
	
	public String getPeopleId() {
		return peopleId;
	}
	public void setPeopleId(String peopleId) {
		this.peopleId = peopleId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getSurname() {
		return surname;
	}
	public void setSurname(String surname) {
		this.surname = surname;
	}
	public String getProfession() {
		return profession;
	}
	public void setProfession(String profession) {
		this.profession = profession;
	}
	public List<Address> getAddress() {
		return address;
	}
	public void setAddress(List<Address> address) {
		this.address = address;
	}
	@Override
	public String toString() {
		return "People [peopleId=" + peopleId + ", name=" + name + ", surname=" + surname + ", profession=" + profession + ", address=" + address + "]";
	}
}
