package it.rcs.people.controller;

import it.rcs.people.model.dto.People;
import io.swagger.annotations.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.constraints.*;
import javax.validation.Valid;
import java.util.List;
import java.util.Map;
@javax.annotation.Generated(value = "io.swagger.codegen.v3.generators.java.SpringCodegen", date = "2021-02-15T16:50:46.876+01:00[Europe/Berlin]")
@Controller
public class PeopleApiController implements PeopleApi {

    private final PeopleApiDelegate delegate;

    @org.springframework.beans.factory.annotation.Autowired
    public PeopleApiController(PeopleApiDelegate delegate) {
        this.delegate = delegate;
    }
    public ResponseEntity<Void> deletePeople(@ApiParam(value = "",required=true) @PathVariable("peopleId") String peopleId
) {
        return delegate.deletePeople(peopleId);
    }

    public ResponseEntity<People> getPeople(@ApiParam(value = "",required=true) @PathVariable("peopleId") String peopleId
) {
        return delegate.getPeople(peopleId);
    }

    public ResponseEntity<List<People>> getPeople() {
        return delegate.getPeople();
    }

    public ResponseEntity<Void> postPeople(@ApiParam(value = "",required=true) @PathVariable("peopleId") String peopleId
,@ApiParam(value = ""  )  @Valid @RequestBody People body
) {
        return delegate.postPeople(peopleId, body);
    }

    public ResponseEntity<Void> putPeoplePeopleId(@ApiParam(value = "",required=true) @PathVariable("peopleId") String peopleId
,@ApiParam(value = ""  )  @Valid @RequestBody People body
) {
        return delegate.putPeoplePeopleId(peopleId, body);
    }

}
