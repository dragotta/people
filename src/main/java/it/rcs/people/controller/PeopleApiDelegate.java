package it.rcs.people.controller;

import it.rcs.people.model.dto.People;
import io.swagger.annotations.*;
import org.springframework.http.ResponseEntity;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;
import java.util.Map;

/**
 * A delegate to be called by the {@link PeopleApiController}}.
 * Implement this interface with a {@link org.springframework.stereotype.Service} annotated class.
 */
@javax.annotation.Generated(value = "io.swagger.codegen.v3.generators.java.SpringCodegen", date = "2021-02-15T16:50:46.876+01:00[Europe/Berlin]")
public interface PeopleApiDelegate {

    /**
     * @see PeopleApi#deletePeople
     */
    ResponseEntity<Void> deletePeople( String  peopleId);

    /**
     * @see PeopleApi#getPeople
     */
    ResponseEntity<People> getPeople( String  peopleId);

    /**
     * @see PeopleApi#getPeople
     */
    ResponseEntity<List<People>> getPeople();

    /**
     * @see PeopleApi#postPeople
     */
    ResponseEntity<Void> postPeople( String  peopleId,
         People  body);

    /**
     * @see PeopleApi#putPeoplePeopleId
     */
    ResponseEntity<Void> putPeoplePeopleId( String  peopleId,
         People  body);

}
