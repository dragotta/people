package it.rcs.people.controller;

import java.util.List;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;

import it.rcs.people.model.dto.People;
import it.rcs.people.repository.PeopleRepository;

@Controller
public class PeopleApiDelegateImpl implements PeopleApiDelegate {
	
	private static final Logger logger = LoggerFactory.getLogger(PeopleApiDelegateImpl.class);
	private final PeopleRepository peopleRepository; 
	
	public PeopleApiDelegateImpl(PeopleRepository peopleRepository) {
		super();
		this.peopleRepository = peopleRepository;
	}

	@Override
	public ResponseEntity<Void> deletePeople(String peopleId) {
		logger.info("DeletePeople");
		peopleRepository.deletePeople(peopleId);
		ResponseEntity<Void> resp= new ResponseEntity<>(HttpStatus.OK);
		logger.info("End DeletePeople");
		return resp;
	}

	@Override
	public ResponseEntity<People> getPeople(String peopleId) {
		logger.info("GetPeople by id");
		People p = null;
		it.rcs.people.entity.People people = peopleRepository.getPeople(peopleId);
		if (people != null)
			p = convertPeopleToDTO(people);
		ResponseEntity<People> resp = new ResponseEntity<>(p,HttpStatus.OK);
		logger.info("End GetPeople by id");
		return resp;
	}

	@Override
	public ResponseEntity<List<People>> getPeople() {
		logger.info("GetPeople");
		List<People> list = null;
		List<it.rcs.people.entity.People> peopleList = peopleRepository.getPeople();
		if (peopleList != null) {
			list = peopleList.stream().map(p -> convertPeopleToDTO(p)).collect(Collectors.toList());
		}
		ResponseEntity<List<People>> resp = new ResponseEntity<>(list,HttpStatus.OK);
		logger.info("End GetPeople");
		return resp;
	}

	@Override
	public ResponseEntity<Void> postPeople(String peopleId, People body) {
		logger.info("InsertPeople");
		it.rcs.people.entity.People people = convertPeopleToEntity(body);
		peopleRepository.insertPeople(people);
		ResponseEntity<Void> resp = new ResponseEntity<>(HttpStatus.OK);
		logger.info("End InsertPeople");
		return resp;
	}

	@Override
	public ResponseEntity<Void> putPeoplePeopleId(String peopleId, People body) {
		logger.info("UpdatePeople");
		it.rcs.people.entity.People people = convertPeopleToEntity(body);
		peopleRepository.updatePeople(people);
		ResponseEntity<Void> resp = new ResponseEntity<>(HttpStatus.OK);
		logger.info("End UpdatePeople");
		return resp;
	}

	private People convertPeopleToDTO(it.rcs.people.entity.People people) {
		People p = new People ();
		p.setPeopleId(people.getPeopleId());
		p.setName(people.getName());
		p.setSurname(people.getSurname());
		p.setProfession(people.getProfession());
		p.setAddress(people.getAddress());
		return p;
	}
	
	private it.rcs.people.entity.People convertPeopleToEntity(People people) {
		it.rcs.people.entity.People p = new it.rcs.people.entity.People();
		p.setPeopleId(people.getPeopleId());
		p.setName(people.getName());
		p.setSurname(people.getSurname());
		p.setProfession(people.getProfession());
		p.setAddress(people.getAddress());
		return p;
	}
}
