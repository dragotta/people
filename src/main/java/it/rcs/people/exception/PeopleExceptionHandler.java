package it.rcs.people.exception;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import com.mongodb.MongoWriteException;

@ControllerAdvice
public class PeopleExceptionHandler {
	
	   @ExceptionHandler(value = MongoEx.class)
	   public ResponseEntity<Object> exception(MongoEx exception) {
	      return new ResponseEntity<>("Error while execute query on db", HttpStatus.BAD_REQUEST);
	   }
	   
	   @ExceptionHandler(value = MongoWriteException.class)
	   public ResponseEntity<Object> exception(MongoWriteException exception) {
	      return new ResponseEntity<>("People object values does not valid", HttpStatus.BAD_REQUEST);
	   }
	   
	   @ExceptionHandler(value = Exception.class)
	   public ResponseEntity<Object> exception(Exception exception) {
	      return new ResponseEntity<>("Generic Exception", HttpStatus.BAD_REQUEST);
	   }
}
