package it.rcs.people.exception;

import com.mongodb.MongoException;

public class MongoEx extends MongoException {

	private static final long serialVersionUID = 1L;

	public MongoEx(String msg) {
		super(msg);
	}
}
