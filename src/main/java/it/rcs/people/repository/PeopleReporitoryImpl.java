package it.rcs.people.repository;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import it.rcs.people.entity.People;

@Repository
public class PeopleReporitoryImpl implements PeopleRepository{
	
	@Autowired
	private MongoTemplate mongoTemplate;

	@Override
	public void insertPeople(People people) {
		mongoTemplate.insert(people);
		
	}

	@Override
	public People getPeople(String peopleId) {
		return mongoTemplate.findById(peopleId, People.class);
	}

	@Override
	public List<People> getPeople() {
		return mongoTemplate.findAll(People.class);
	}

	@Override
	public void deletePeople(String peopleId) {
		Query query = new Query();
		query.addCriteria(Criteria.where("peopleId").is(peopleId));
		mongoTemplate.remove(query, People.class);
		
	}

	@Override
	public void updatePeople(People people) {
		mongoTemplate.save(people);		
	}
}
