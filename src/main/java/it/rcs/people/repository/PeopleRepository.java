package it.rcs.people.repository;

import java.util.List;

import it.rcs.people.entity.People;

public interface PeopleRepository {
	
	public void insertPeople (People people);
	public People getPeople (String peopleId);
	public List<People> getPeople ();
	public void deletePeople (String peopleId);
	public void updatePeople (People people);

}
